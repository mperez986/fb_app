This is a repository of two applications:

1) fb_app_mis is a JavaScript/Java web application which runs the link that lets 
users log in to the application.

2) fb_data_processing_mis is a Java application which takes the input file
people.txt which is a csv, each line consisting of an id and access token. 
The application goes through the access tokens and prints out the desired data
to the console.

Recommended environment is Eclipse IDE for Java EE Developers